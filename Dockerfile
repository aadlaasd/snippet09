FROM golang:1.15

RUN mkdir /app

ADD . /app/

WORKDIR /app

EXPOSE 5433

EXPOSE 8080

FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=0 /app .
CMD go run ./cmd/web

