package postgres

import (
	"context"
	"github.com/jackc/pgx"
	_ "github.com/jackc/pgx"
	_ "github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"se09.com/pkg/models"
	"strconv"
	"time"
)
// Define a SnippetModel type which wraps a sql.DB connection pool.
type SnippetModel struct {
	DB *pgxpool.Pool
}

func (m *SnippetModel) Get(id int) (*models.Snippet, error) {
	stmt := "SELECT id, title, content, created, expires FROM snippets WHERE expires > CURRENT_TIMESTAMP AND id=$1"
	row := m.DB.QueryRow(context.Background(),stmt, id)
	// Initialize a pointer to a new zeroed Snippet struct.
	s := &models.Snippet{}
	// Use row.Scan() to copy the values from each field in sql.Row to the
	// corresponding field in the Snippet struct. Notice that the arguments
	// to row.Scan are *pointers* to the place you want to copy the data into,
	// and the number of arguments must be exactly the same as the number of
	// columns returned by your statement.
	err := row.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		// If the query returns no rows, then row.Scan() will return a
		// sql.ErrNoRows error. We use the errors.Is() function check for that
		// error specifically, and return our own models.ErrNoRecord error
		// instead.
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	// If everything went OK then return the Snippet object.
	return s, nil
}


func (m *SnippetModel) Latest() ([]*models.Snippet, error) {
	stmt := "SELECT id, title, content, created, expires FROM snippets " +
		"WHERE expires > CURRENT_TIMESTAMP ORDER BY created DESC LIMIT 10"
	rows, err := m.DB.Query(context.Background(),stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var snippets []*models.Snippet
	for rows.Next() {
		s := &models.Snippet{}
		err = rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if err != nil {
			return nil, err
		}
		snippets = append(snippets, s)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}

	return snippets, err
}

func (m *SnippetModel) Insert(title, content, expires string) (int, error) {
	stmt :="INSERT INTO snippets (title, content, created, expires)"+
		"VALUES($1, $2, $3, $4) RETURNING id"
	date, error := strconv.Atoi(expires)
	if error != nil {
		return 0, error
	}
	created := time.Now()
	dateAdd := time.Now().AddDate(0, 0, date)

	result, err := m.DB.Exec(context.Background(),stmt, title, content,created, dateAdd)
	if err != nil {
		return 0, err
	}
	// Use the LastInsertId() method on the result object to get the ID of our
	// newly inserted record in the snippets table.
	id:=result.RowsAffected()
	// The ID returned has the type int64, so we convert it to an int type
	// before returning.
	return int(id), nil
}